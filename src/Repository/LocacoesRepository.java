/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Entities.Carro;
import Entities.Locacao;
import Entities.Pessoa;
import Models.LocacoesTableModel;
import Utils.HibernateUtils;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import locadora.JFLocacoes;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author João Stein
 */
public class LocacoesRepository {

    public void atualizaModel(String cpf, String placa, int id) {
        LocacoesTableModel model = (LocacoesTableModel) JFLocacoes.jTValores.getModel();
        model.limpar();
        model.addLista(listar(cpf, placa, id));
        JFLocacoes.jTValores.setModel(model);
    }

    public List<Locacao> listar(String cpf, String placa, int id) {
        Session session = HibernateUtils.openSession();
        try {
            String sql = "select * from locacao "
                    + "inner join carros on(carros_id=carros.id) "
                    + "inner join pessoas on(pessoas_id=pessoas.id) "
                    + "WHERE carros.placa ILIKE :placa AND pessoas.cpf ILIKE :cpf ";
            if (id != 0) {
                sql += " and pessoas.id = " + id;
            }
            sql += "order by locacao.data_devolucao desc, locacao.data_locacao  desc";
            Query query = session.createSQLQuery(sql)
                    .addEntity(Locacao.class)
                    .setParameter("placa", "%" + placa + "%")
                    .setParameter("cpf", "%" + cpf + "%");
            return query.list();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public Locacao buscaPorId(int id) {
        String sql = "select * from locacao "
                + "inner join carros on(carros_id=carros.id) "
                + "inner join pessoas on(pessoas_id=pessoas.id) "
                + "where locacao.id = :id ";
        Session session = HibernateUtils.openSession();
        try {
            Query query = session.createSQLQuery(sql)
                    .addEntity(Locacao.class)
                    .setParameter("id", id);
            return (Locacao) query.uniqueResult();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void incluir(Carro carro, Pessoa pessoa, Date data) {
        Session session = HibernateUtils.openSession();
        try {
            if (!carro.isAtivo()) {
                throw new Exception("Carro inativo");
            }
            if (carro.getStatus() == 1) {
                throw new Exception("Carro locado");
            }
            carro.setStatus(1);
            Locacao locacao = new Locacao();
            locacao.setCarrosId(carro);
            locacao.setPessoasId(pessoa);
            locacao.setDataLocacao(data);
            locacao.setKmLocacao(carro.getKm());
            session.getTransaction().begin();
            session.update(carro);
            session.saveOrUpdate(locacao);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void retornar(Locacao locacao, Date data, Double preco, Double km) {
        Session session = HibernateUtils.openSession();
        try {
            Carro carro = locacao.getCarrosId();
            if (data.getTime() < locacao.getDataLocacao().getTime()) {
                throw new Exception("Data inválida");
            }
            if (carro.getKm() > km) {
                throw new Exception("Kilometragem inválida");
            }
            carro.setStatus(0);
            carro.setKm(km);
            locacao.setDataDevolucao(data);
            locacao.setKmDevolucao(km);
            locacao.setPreco(preco);
            session.getTransaction().begin();
            session.update(carro);
            session.update(locacao);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
    
}
