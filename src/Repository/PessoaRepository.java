/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Entities.Locacao;
import Entities.Pessoa;
import Models.PessoasTableModel;
import Utils.HibernateUtils;
import Utils.ValidationUtils;
import java.math.BigInteger;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import locadora.JFPessoas;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author João Stein
 */
public class PessoaRepository {

    public void atualizaModel(String nome, String cpf, String rg) {
        PessoasTableModel model = (PessoasTableModel) JFPessoas.jTValores.getModel();
        model.limpar();
        model.addLista(listar(nome, rg, cpf));
        JFPessoas.jTValores.setModel(model);
    }

    public List<Pessoa> listar(String nome, String rg, String cpf) {
        Session session = HibernateUtils.openSession();
        try {
            String sql = "SELECT * FROM pessoas "
                    + " WHERE nome ILIKE :nome AND cpf ILIKE :cpf AND rg ILIKE :rg";
            Query query = session.createSQLQuery(sql)
                    .addEntity(Pessoa.class)
                    .setParameter("nome", "%" + nome + "%")
                    .setParameter("rg", "%" + rg + "%")
                    .setParameter("cpf", "%" + cpf + "%");
            return query.list();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void incluir(Pessoa pessoa) {
        Session session = HibernateUtils.openSession();
        try {
            if (!ValidationUtils.isCpfValid(pessoa.getCpf())) {
                throw new Exception("CPF Inválido");
            }
            boolean cpfJaCadastrado = session.createSQLQuery("select * from pessoas where cpf = :cpf")
                    .addEntity(Pessoa.class)
                    .setParameter("cpf", pessoa.getCpf())
                    .uniqueResult() != null;
            if (cpfJaCadastrado) {
                throw new Exception("CPF já cadastrado");
            }
            boolean cnhJaCadastrada = session.createSQLQuery("select * from pessoas where carteira_motorista = :cnh")
                    .addEntity(Pessoa.class)
                    .setParameter("cnh", pessoa.getCarteiraMotorista())
                    .uniqueResult() != null;
            if (cnhJaCadastrada) {
                throw new Exception("CNH já cadastrado");
            }
            boolean rgJaCadastrado = session.createSQLQuery("select * from pessoas where rg = :rg")
                    .addEntity(Pessoa.class)
                    .setParameter("rg", pessoa.getRg())
                    .uniqueResult() != null;
            if (rgJaCadastrado) {
                throw new Exception("RG já cadastrado");
            }
            if (pessoa.getIdade() < 18) {
                throw new Exception("Idade inválida");
            }
            session.getTransaction().begin();
            session.saveOrUpdate(pessoa);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public Pessoa buscaPorId(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Query query = session.createSQLQuery("SELECT * FROM pessoas WHERE id = :id")
                    .addEntity(Pessoa.class)
                    .setParameter("id", id);
            return (Pessoa) query.uniqueResult();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void alterar(Pessoa pessoa) {
        Session session = HibernateUtils.openSession();
        try {
            if (pessoa.getIdade() < 18) {
                throw new Exception("Idade inválida");
            }
            session.getTransaction().begin();
            session.saveOrUpdate(pessoa);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void excluir(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Pessoa pessoa = buscaPorId(id);
            boolean possuiRegistros = !session.createSQLQuery("select * from locacao where pessoas_id = :id and data_devolucao is null")
                    .addEntity(Locacao.class)
                    .setParameter("id", pessoa.getId())
                    .list().isEmpty();
            if (possuiRegistros) {
                throw new Exception("Não foi possível excluir pessoa, ela possui locações");
            }
            session.getTransaction().begin();
            session.delete(pessoa);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

}
