/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Entities.Carro;
import Models.CarrosTableModel;
import Models.MarcaTableModel;
import Utils.HibernateUtils;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import locadora.JFCarros;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author João Stein
 */
public class CarroRepository {
    
    public void atualizaModel(String nome, String placa, String marca) {
        CarrosTableModel model = (CarrosTableModel) JFCarros.jTCarros.getModel();
        model.limpar();
        model.addLista(listar(nome, placa, marca));
        JFCarros.jTCarros.setModel(model);
    }
    
    public List<Carro> listar(String nome, String placa, String marca) {
        Session session = HibernateUtils.openSession();
        try {
            String sql = "SELECT * FROM carros " +
                    "INNER JOIN marca ON(carros.marca_id=marca.id) "+
                    " WHERE carros.placa ILIKE :placa AND marca.nome ILIKE :marca AND carros.nome ILIKE :nome";
            Query query = session.createSQLQuery(sql)
                    .addEntity(Carro.class)
                    .setParameter("nome", "%" + nome + "%")
                    .setParameter("placa", "%" + placa + "%")
                    .setParameter("marca", "%" + marca + "%");
            return query.list();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
//    public int proximaKey(){
//        Session session = HibernateUtils.openSession();
//        try {
//            return session.createSQLQuery("select ")
//        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
//            throw new Error(ex.getMessage());
//        } finally {
//            session.close();
//        }
//    }
    public void incluir(Carro carro){
        Session session = HibernateUtils.openSession();
        try {
            session.getTransaction().begin();
            session.saveOrUpdate(carro);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
    public void alterar(Carro carro){
        Session session = HibernateUtils.openSession();
        try {
            session.getTransaction().begin();
            session.saveOrUpdate(carro);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
    public Carro buscaPorId(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Query query = session.createSQLQuery("SELECT * FROM carros WHERE id = :id")
                    .addEntity(Carro.class)
                    .setParameter("id", id);
            return (Carro) query.uniqueResult();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
    public void excluir(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Carro carro = buscaPorId(id);
            session.getTransaction().begin();
            session.delete(carro);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
     public List<Carro> listarAtivos() {
        Session session = HibernateUtils.openSession();
        try {
            String sql = "SELECT * FROM carros " +
                    "INNER JOIN marca ON(carros.marca_id=marca.id) "+
                    " WHERE carros.ativo = true and status = 0";
            Query query = session.createSQLQuery(sql)
                    .addEntity(Carro.class);
                    
            return query.list();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
}
