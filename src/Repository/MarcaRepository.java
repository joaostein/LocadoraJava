/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Entities.Marca;
import Models.MarcaTableModel;
import Utils.HibernateUtils;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import locadora.JFMarcas;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author João Stein
 */
public class MarcaRepository {

    public void atualizaModel(String descricao) {
        MarcaTableModel model = (MarcaTableModel) JFMarcas.jTValores.getModel();
        model.limpar();
        model.addLista(listar(descricao));
        JFMarcas.jTValores.setModel(model);
    }

    public List<Marca> listar(String nome) {
        Session session = HibernateUtils.openSession();
        try {
            Query query = session.createSQLQuery("SELECT * FROM marca WHERE Nome ILIKE :nome ORDER BY Id")
                    .addEntity(Marca.class)
                    .setParameter("nome", "%" + nome + "%");
            return query.list();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void incluir(String nome) {
        Session session = HibernateUtils.openSession();
        try {
            session.getTransaction().begin();
            Query query = session.createSQLQuery("INSERT INTO marca (nome) values (:nome)")
                    .setParameter("nome", nome);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public Marca buscaPorId(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Query query = session.createSQLQuery("SELECT * FROM marca WHERE id = :id")
                    .addEntity(Marca.class)
                    .setParameter("id", id);
            return (Marca) query.uniqueResult();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void excluir(int id) {
        Session session = HibernateUtils.openSession();
        try {
            Marca marca = buscaPorId(id);
            session.getTransaction().begin();
            session.delete(marca);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void alterar(Marca marca) {
        Session session = HibernateUtils.openSession();
        try {
            session.getTransaction().begin();
            session.saveOrUpdate(marca);
            session.getTransaction().commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new Error(ex.getMessage());
        } finally {
            session.close();
        }
    }
}
