/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author João Stein
 */
@Entity
@Table(name = "locacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locacao.findAll", query = "SELECT l FROM Locacao l")
    , @NamedQuery(name = "Locacao.findById", query = "SELECT l FROM Locacao l WHERE l.id = :id")
    , @NamedQuery(name = "Locacao.findByDataLocacao", query = "SELECT l FROM Locacao l WHERE l.dataLocacao = :dataLocacao")
    , @NamedQuery(name = "Locacao.findByDataDevolucao", query = "SELECT l FROM Locacao l WHERE l.dataDevolucao = :dataDevolucao")
    , @NamedQuery(name = "Locacao.findByPreco", query = "SELECT l FROM Locacao l WHERE l.preco = :preco")
    , @NamedQuery(name = "Locacao.findByKmLocacao", query = "SELECT l FROM Locacao l WHERE l.kmLocacao = :kmLocacao")
    , @NamedQuery(name = "Locacao.findByKmDevolucao", query = "SELECT l FROM Locacao l WHERE l.kmDevolucao = :kmDevolucao")})
public class Locacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "locacao_sequence")
    @SequenceGenerator(name="locacao_sequence", sequenceName = "locacao_sequence")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "data_locacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataLocacao;
    @Column(name = "data_devolucao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataDevolucao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "preco")
    private Double preco;
    @Basic(optional = false)
    @Column(name = "km_locacao")
    private Double kmLocacao;
    @Column(name = "km_devolucao")
    private Double kmDevolucao;
    @JoinColumn(name = "carros_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Carro carrosId;
    @JoinColumn(name = "pessoas_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pessoa pessoasId;

    public Locacao() {
    }

    public Locacao(Integer id) {
        this.id = id;
    }

    public Locacao(Integer id, Date dataLocacao, Date dataDevolucao, Double preco, Double kmLocacao, Double kmDevolucao) {
        this.id = id;
        this.dataLocacao = dataLocacao;
        this.dataDevolucao = dataDevolucao;
        this.preco = preco;
        this.kmLocacao = kmLocacao;
        this.kmDevolucao = kmDevolucao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(Date dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public Double getKmLocacao() {
        return kmLocacao;
    }

    public void setKmLocacao(Double kmLocacao) {
        this.kmLocacao = kmLocacao;
    }

    public Double getKmDevolucao() {
        return kmDevolucao;
    }

    public void setKmDevolucao(Double kmDevolucao) {
        this.kmDevolucao = kmDevolucao;
    }

    public Carro getCarrosId() {
        return carrosId;
    }

    public void setCarrosId(Carro carrosId) {
        this.carrosId = carrosId;
    }

    public Pessoa getPessoasId() {
        return pessoasId;
    }

    public void setPessoasId(Pessoa pessoasId) {
        this.pessoasId = pessoasId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locacao)) {
            return false;
        }
        Locacao other = (Locacao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Locacao[ id=" + id + " ]";
    }
    
}
