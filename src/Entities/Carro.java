/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author João Stein
 */
@Entity
@Table(name = "carros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carro.findAll", query = "SELECT c FROM Carro c")
    , @NamedQuery(name = "Carro.findById", query = "SELECT c FROM Carro c WHERE c.id = :id")
    , @NamedQuery(name = "Carro.findByPlaca", query = "SELECT c FROM Carro c WHERE c.placa = :placa")
    , @NamedQuery(name = "Carro.findByChassi", query = "SELECT c FROM Carro c WHERE c.chassi = :chassi")
    , @NamedQuery(name = "Carro.findByRenavan", query = "SELECT c FROM Carro c WHERE c.renavan = :renavan")
    , @NamedQuery(name = "Carro.findByKm", query = "SELECT c FROM Carro c WHERE c.km = :km")})
public class Carro implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carros_sequence")
    @SequenceGenerator(name="carros_sequence", sequenceName = "carros_sequence")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @Column(name = "chassi")
    private String chassi;
    @Column(name = "nome")
    private String nome;
    @Column(name = "status")
    private Integer status;
    @Basic(optional = false)
    @Column(name = "renavan")
    private String renavan;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "km")
    private Double km;
    @Column(name = "valor_dia")
    private Double valorDia;
    @Column(name = "ativo")
    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carrosId")
    private Collection<Locacao> locacaoCollection;
    @JoinColumn(name = "marca_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Marca marcaId;

    public Carro() {
    }

    public Carro(Integer id) {
        this.id = id;
    }
 public Carro(String nome, String placa, String chassi, String renavan, Double km, Double valorDiario, Marca marca, boolean ativo) {
        this.placa = placa;
        this.nome = nome;
        this.chassi = chassi;
        this.renavan = renavan;
        this.km = km;
        this.marcaId = marca;
        this.valorDia = valorDiario;
        this.ativo = ativo;
        this.status = 0;
    }
    public Carro(Integer id, String placa, String chassi, String renavan, Double km) {
        this.id = id;
        this.placa = placa;
        this.chassi = chassi;
        this.renavan = renavan;
        this.km = km;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        String oldPlaca = this.placa;
        this.placa = placa;
        changeSupport.firePropertyChange("placa", oldPlaca, placa);
    }

    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        String oldChassi = this.chassi;
        this.chassi = chassi;
        changeSupport.firePropertyChange("chassi", oldChassi, chassi);
    }

    public String getRenavan() {
        return renavan;
    }

    public void setRenavan(String renavan) {
        String oldRenavan = this.renavan;
        this.renavan = renavan;
        changeSupport.firePropertyChange("renavan", oldRenavan, renavan);
    }

    public Double getKm() {
        return km;
    }

    public void setKm(Double km) {
        Double oldKm = this.km;
        this.km = km;
        changeSupport.firePropertyChange("km", oldKm, km);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @XmlTransient
    public Collection<Locacao> getLocacaoCollection() {
        return locacaoCollection;
    }

    public void setLocacaoCollection(Collection<Locacao> locacaoCollection) {
        this.locacaoCollection = locacaoCollection;
    }

    public Marca getMarcaId() {
        return marcaId;
    }

    public void setMarcaId(Marca marcaId) {
        Marca oldMarcaId = this.marcaId;
        this.marcaId = marcaId;
        changeSupport.firePropertyChange("marcaId", oldMarcaId, marcaId);
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Double getValorDia() {
        return valorDia;
    }

    public void setValorDia(Double valorDia) {
        this.valorDia = valorDia;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carro)) {
            return false;
        }
        Carro other = (Carro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Carro[ id=" + id + ", marca="+marcaId.getNome()+" ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
