/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author João Stein
 */
import Entities.Pessoa;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class PessoasTableModel extends AbstractTableModel {

    private List<Pessoa> pessoas;
    private String[] colunas = new String[]{
        "Id", "CPF", "Nome", "RG"};

    public PessoasTableModel(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public PessoasTableModel() {
        this.pessoas = new ArrayList<Pessoa>();
    }

    public int getRowCount() {
        return pessoas.size();
    }

    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Pessoa pessoaSelecionada = pessoas.get(rowIndex);
        String valueObject = null;
        switch (columnIndex) {
            case 0:
                valueObject = pessoaSelecionada.getId().toString();
                break;
            case 1:
                valueObject = pessoaSelecionada.getCpf();
                break;
            case 2:
                valueObject = pessoaSelecionada.getNome();
                break;
            case 3:
                valueObject = pessoaSelecionada.getRg();
                break;
            default:
                System.err.println("Índice inválido para propriedade do bean Carro");
        }

        return valueObject;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Pessoa getPessoa(int indiceLinha) {
        return pessoas.get(indiceLinha);
    }

    public void addPessoa(Pessoa pessoa) {
        pessoas.add(pessoa);

        int ultimoIndice = getRowCount() - 1;

        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }

    public void removePessoa(int indiceLinha) {
        pessoas.remove(indiceLinha);

        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    public void addLista(List<Pessoa> novasPessoas) {

        int tamanhoAntigo = getRowCount();
        pessoas.addAll(novasPessoas);
        fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
    }

    public void limpar() {
        pessoas.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return pessoas.isEmpty();
    }

}
