/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author João Stein
 */
import Entities.Marca;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class MarcaTableModel extends AbstractTableModel {

    private List<Marca> marcas;
    private String[] colunas = new String[]{
        "Id", "Nome"};

    public MarcaTableModel(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public MarcaTableModel() {
        this.marcas = new ArrayList<Marca>();
    }

    public int getRowCount() {
        return marcas.size();
    }

    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Marca marcaSelecionada = marcas.get(rowIndex);
        String valueObject = null;
        switch (columnIndex) {
            case 0:
                valueObject = marcaSelecionada.getId().toString();
                break;
            case 1:
                valueObject = marcaSelecionada.getNome();
                break;
            default:
                System.err.println("Índice inválido para propriedade do bean Marca");
        }

        return valueObject;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Marca getMarca(int indiceLinha) {
        return marcas.get(indiceLinha);
    }

    public void addMarca(Marca marca) {
        marcas.add(marca);

        int ultimoIndice = getRowCount() - 1;

        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }

    public void removeMarca(int indiceLinha) {
        marcas.remove(indiceLinha);

        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    public void addLista(List<Marca> novasMarcas) {

        int tamanhoAntigo = getRowCount();
        marcas.addAll(novasMarcas);
        fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
    }

    public void limpar() {
        marcas.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return marcas.isEmpty();
    }

}
