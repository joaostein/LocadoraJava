/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author João Stein
 */
import Entities.Locacao;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class LocacoesTableModel extends AbstractTableModel {

    private List<Locacao> locacoes;
    private String[] colunas = new String[]{
        "Id", "Locador", "Carro", "Data de locação", "Data de devolução", "Valor"};

    public LocacoesTableModel(List<Locacao> locacoes) {
        this.locacoes = locacoes;
    }

    public LocacoesTableModel() {
        this.locacoes = new ArrayList<Locacao>();
    }

    public int getRowCount() {
        return locacoes.size();
    }

    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Locacao locacaoSelecionada = locacoes.get(rowIndex);
        String valueObject = null;
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        switch (columnIndex) {
            case 0:
                valueObject = locacaoSelecionada.getId().toString();
                break;
            case 1:
                valueObject = locacaoSelecionada.getPessoasId().getCpf() + " - " + locacaoSelecionada.getPessoasId().getNome();
                break;
            case 2:
                valueObject = locacaoSelecionada.getCarrosId().getPlaca() + " - " + locacaoSelecionada.getCarrosId().getNome();
                break;
            case 3:
                valueObject = dt1.format(locacaoSelecionada.getDataLocacao()) ;
                break;
            case 4:
                valueObject = locacaoSelecionada.getDataDevolucao()== null ? "" :dt1.format(locacaoSelecionada.getDataDevolucao());
                break;
            case 5:
                valueObject =locacaoSelecionada.getPreco()== null ? "" :locacaoSelecionada.getPreco().toString();
                break;
            default:
                System.err.println("Índice inválido para propriedade do bean Locacao");
        }

        return valueObject;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Locacao getLocacao(int indiceLinha) {
        return locacoes.get(indiceLinha);
    }

    public void addLocacao(Locacao locacao) {
        locacoes.add(locacao);

        int ultimoIndice = getRowCount() - 1;

        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }

    public void removeLocacao(int indiceLinha) {
        locacoes.remove(indiceLinha);

        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    public void addLista(List<Locacao> novasLocacoes) {

        int tamanhoAntigo = getRowCount();
        locacoes.addAll(novasLocacoes);
        fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
    }

    public void limpar() {
        locacoes.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return locacoes.isEmpty();
    }

}
