/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author João Stein
 */
import Entities.Carro;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class CarrosTableModel extends AbstractTableModel {

    private List<Carro> carros;
    private String[] colunas = new String[]{
        "Id", "Nome", "Marca", "Placa", "Status"};

    public CarrosTableModel(List<Carro> carros) {
        this.carros = carros;
    }

    public CarrosTableModel() {
        this.carros = new ArrayList<Carro>();
    }

    public int getRowCount() {
        return carros.size();
    }

    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Carro carroSelecionado = carros.get(rowIndex);
        String valueObject = null;
        switch (columnIndex) {
            case 0:
                valueObject = carroSelecionado.getId().toString();
                break;
            case 1:
                valueObject = carroSelecionado.getNome();
                break;
            case 2:
                valueObject = carroSelecionado.getMarcaId().getNome();
                break;
            case 3:
                valueObject = carroSelecionado.getPlaca();
                break;
            case 4:
                valueObject = carroSelecionado.getStatus() == 0 ? "Disponível": "Alugado";
                break;
            default:
                System.err.println("Índice inválido para propriedade do bean Carro");
        }

        return valueObject;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Carro getCarro(int indiceLinha) {
        return carros.get(indiceLinha);
    }

    public void addCarro(Carro carro) {
        carros.add(carro);

        int ultimoIndice = getRowCount() - 1;

        fireTableRowsInserted(ultimoIndice, ultimoIndice);
    }

    public void removeCarro(int indiceLinha) {
        carros.remove(indiceLinha);

        fireTableRowsDeleted(indiceLinha, indiceLinha);
    }

    public void addLista(List<Carro> novosCarros) {

        int tamanhoAntigo = getRowCount();
        carros.addAll(novosCarros);
        fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
    }

    public void limpar() {
        carros.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return carros.isEmpty();
    }

}
