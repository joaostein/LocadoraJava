/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author João Stein
 */
public class HibernateUtils {

    private static final SessionFactory SESSION_FACTORY = buildSessionFactory();
    
    
    
    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            System.out.println("Create the SessionFactory from hibernate.cfg.xml");
            return new Configuration().configure().buildSessionFactory();
        } catch (Exception ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw ex;
        }
    }
    public static Session openSession(){
        System.out.println("Creating session");
        return SESSION_FACTORY.openSession();
    }
}
