-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-08-24 00:31:52.052

-- sequences
-- Sequence: carros_sequence
CREATE SEQUENCE carros_sequence
      INCREMENT BY 1
      MINVALUE 1
      NO MAXVALUE
      START WITH 1
      NO CYCLE
;

-- Sequence: locacao_sequence
CREATE SEQUENCE locacao_sequence
      INCREMENT BY 1
      MINVALUE 1
      NO MAXVALUE
      START WITH 1
      NO CYCLE
;

-- Sequence: marca_sequence
CREATE SEQUENCE marca_sequence
      INCREMENT BY 1
      MINVALUE 1
      NO MAXVALUE
      START WITH 1
      NO CYCLE
;

-- Sequence: pessoas_sequence
CREATE SEQUENCE pessoas_sequence
      INCREMENT BY 1
      MINVALUE 1
      NO MAXVALUE
      START WITH 1
      NO CYCLE
;






-- tables
-- Table: carros
CREATE TABLE carros (
    id int  NOT NULL DEFAULT nextval('carros_sequence'),
    placa text  NOT NULL,
    chassi text  NOT NULL,
    renavan text  NOT NULL,
    nome text,
    km decimal(12,2)  NOT NULL,
    status int  NOT NULL,
    marca_id int  NOT NULL,
    valor_dia numeric(12,2),
    ativo boolean,
    CONSTRAINT carros_pk PRIMARY KEY (id)
);

-- Table: locacao
CREATE TABLE locacao (
    id int  NOT NULL DEFAULT nextval('locacao_sequence'),
    data_locacao timestamp  NOT NULL,
    data_devolucao timestamp ,
    carros_id int  NOT NULL,
    pessoas_id int  NOT NULL,
    preco decimal(12,2) ,
    km_locacao decimal(12,2)  NOT NULL,
    km_devolucao decimal(12,2),
    CONSTRAINT locacao_pk PRIMARY KEY (id)
);

-- Table: marca
CREATE TABLE marca (
    id int  NOT NULL DEFAULT nextval('marca_sequence'),
    nome text  NOT NULL,
    CONSTRAINT marca_pk PRIMARY KEY (id)
);



-- Table: pessoas
CREATE TABLE pessoas (
    id int  NOT NULL DEFAULT nextval('pessoas_sequence'),
    nome text  NOT NULL,
    cpf text  NOT NULL,
    rg text  NOT NULL,
    idade int  NOT NULL,
    carteira_motorista text  NOT NULL,
    CONSTRAINT pessoas_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: carros_marca (table: carros)
ALTER TABLE carros ADD CONSTRAINT carros_marca
    FOREIGN KEY (marca_id)
    REFERENCES marca (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: locacao_carros (table: locacao)
ALTER TABLE locacao ADD CONSTRAINT locacao_carros
    FOREIGN KEY (carros_id)
    REFERENCES carros (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: locacao_pessoas (table: locacao)
ALTER TABLE locacao ADD CONSTRAINT locacao_pessoas
    FOREIGN KEY (pessoas_id)
    REFERENCES pessoas (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


